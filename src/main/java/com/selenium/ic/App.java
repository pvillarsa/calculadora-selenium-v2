package com.selenium.ic;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

/**
 * Hello world!
 *
 */
public class App 
{
	private static String URL;
	private static String PATH_CHROME_DRIVER;
	private static WebDriver driver;
	
    public static void main( String[] args )
    {
    	
    	URL = args[0];
    	PATH_CHROME_DRIVER = args[1];
    	
    	System.setProperty("webdriver.chrome.driver", PATH_CHROME_DRIVER);
		ChromeOptions options = new ChromeOptions();
		options.addArguments("--headless");
		options.addArguments("--no-sandbox");
		//options.addArguments("disable-dev-shm-usage");
		options.addArguments("acceptInsecureCerts=true");
		driver = new ChromeDriver(options);
		 
    	System.out.println("-------------------------------");
    	System.out.println("URL: " + URL);
    	System.out.println("PATH_CHROME_DRIVER: " + PATH_CHROME_DRIVER);
    	System.out.println("-------------------------------");
    	driver.navigate().to(URL);
    	boolean result = false;
    	
    	result = testSuma1();
    	result = result && testSuma2();
    	result = result && testResta3();
    	
    	closedDriver();
    	System.out.println("-------------------------------");
    	System.out.println("Tests: " + (result ? "OK" : "NOK" ));
    	System.exit(result ? 0 : -1);   	
    }
    
 public static boolean testSuma1() { 
	 boolean returnTest = false;
	 try {
	 	driver.findElement(By.id("btn_reiniciar")).click();
    	driver.findElement(By.id("btn_n2")).click();
    	driver.findElement(By.id("btn_mas")).click();
    	driver.findElement(By.id("btn_n3")).click();
    	driver.findElement(By.id("btn_resultado")).click();
    	returnTest = "5".equals(driver.findElement(By.id("entrada")).getAttribute("value"));
	 }catch (Exception e){
		 System.out.println(e.toString());
	 }
    	
    	return assertOk("testSuma1", returnTest); 
    }
 
 public static boolean testSuma2() { 
	 boolean returnTest = false;
	 try {
		driver.findElement(By.id("btn_reiniciar")).click();
	 	driver.findElement(By.id("btn_n5")).click();
	 	driver.findElement(By.id("btn_n8")).click();
	 	driver.findElement(By.id("btn_mas")).click();
	 	driver.findElement(By.id("btn_n9")).click();
	 	driver.findElement(By.id("btn_resultado")).click();
 	
	 	returnTest = "67".equals(driver.findElement(By.id("entrada")).getAttribute("value"));
	 }catch (Exception e){
		 System.out.println(e.toString());
	 }
	 
	 return assertOk("testSuma2", returnTest); 
 }
 
 public static boolean testResta3() { 
	boolean returnTest = false;
	try {
		driver.findElement(By.id("btn_reiniciar")).click();
	 	driver.findElement(By.id("btn_n5")).click();
	 	driver.findElement(By.id("btn_n8")).click();
	 	driver.findElement(By.id("btn_menos")).click();
	 	driver.findElement(By.id("btn_n9")).click();
	 	driver.findElement(By.id("btn_resultado")).click();
	 	returnTest = "49".equals(driver.findElement(By.id("entrada")).getAttribute("value"));

	}catch (Exception e){
		 System.out.println(e.toString());
	}
	 
	 return assertOk("testResta3", returnTest); 
 }
    
    public static boolean assertOk(String method, boolean validation) {
    	System.out.println(method + "()" + (validation ? " - OK": " - NOK"));
    	return validation;    			
    }
    
    private static void closedDriver() {
    	driver.close(); 
        driver.quit();
    }
    
}
